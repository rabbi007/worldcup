// DEBUG=true is development mode

var DOMAIN_URL="http://admin.gothibd.com/";
//  var DOMAIN_URL="http://192.168.0.103:8000/";

export const SYSTEM_API_KEY = "048d53ad-GothiAdmin-d9bf-490f-ae78-ece682795a99";



export const LOGIN_URL  = DOMAIN_URL+"login/";
export const LOGOUT_URL = DOMAIN_URL+"logout/";

export const LIST_BUNDLE = "https://support.manadrdev.com/docker/manadr-bundles";

// CME
export const CME_API_URL = "https://cme.manadr.com/v1";
export const CME_STAGING_API_URL = "https://cme.staging.manadrdev.com/v1";

// Appointment
export const AUTHENTICATION_API_URL = "https://api.manadr.com/api/v1.3";
export const AUTHENTICATION_STAGING_API_URL = "https://appointment.staging.manadrdev.com/api/v1.3";
export const APPOINTMENT_X_API_KEY = "048d53ad-d9bf-490f-ae78-ece682795a99";
