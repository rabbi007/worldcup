import React, { Component } from "react";
import Agent from "../../components/CreateAgent";

export default class AgentContainer extends Component {
  render() {
    return (
      <Agent navigation={this.props.navigation}/>
    );
  }
}

