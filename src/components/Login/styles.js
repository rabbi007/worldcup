export default {
  container: {
    backgroundColor: "#fff"
  },
  logo: {
    marginTop: 104,
    alignSelf: "center",
    width: 150,
    height: 50
  },
  textLogo: {
    marginTop: 18,
    alignSelf: "center",
    fontSize: 16
  },
  containerForm: {
    flex: 1,
    marginLeft: 32,
    marginRight: 32
  },
  contentForm: {
    marginTop: 46
  },
  buttonLogin: {
    marginTop: 50
  },
  buttonAgent:{
    marginTop:10,
    backgroundColor: '#13ea6a'
  },
  textLogin: {
    fontSize: 19
  },
  containerEnv: {
    alignItems: "center",
    marginTop: 20
  },
  textChangeEnv: {
    marginTop: 5
  }
};
