import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Text,
  Body,
  Left,
  Button,
  Icon,
  Title,
  Right,
  View,
  Form,
  Picker,
  InputGroup,
  Input,
  Item,
  Textarea,
} from "native-base";
import styles from "./styles";
import { ProgressSteps, ProgressStep } from 'react-native-progress-steps';
import { Image, ScrollView,} from "react-native";
import DatePicker from 'react-native-datepicker';
import ImagePicker from "react-native-image-picker";
import {  required, alphaNumeric } from "../../containers/LoginContainer/validators";
 import moment from "../Partner";

export default class Agent extends Component {

  constructor(props) {
    super(props);
    this.state = {date:""}
    selected2: undefined
    first_name:''
    email:''
    contact_no:''
    nid_or_passport:''
    business_name:''
    trade_licenseno:''
    regional:''
    permanent_address:''
    present_address:''
    ref1_name:''
    ref1_address:''
    ref1_contact_no:''
    ref1_relation:''
    agent_rcv_amount:''
    password:''

  }
  state={
    agentSource:'',
    tdlicenseSource:'',
    agreementSource:'',
    nidSource:'',
    aFormSource:'',
  }
  selectAgent=async () => {
    ImagePicker.showImagePicker({onData: true,mediaType:'photo'},(response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {


        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          agentSource: response.uri,
        });
      }
    });
  };
  selectAgentTdLicense=async () => {
    ImagePicker.showImagePicker({onData: true,mediaType:'photo'},(response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {


        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          tdlicenseSource: response.uri,
        });
      }
    });
  };
  selectAgreementCopy=async () => {
    ImagePicker.showImagePicker({onData: true,mediaType:'photo'},(response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {


        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          agreementSource: response.uri,
        });
      }
    });
  };
  selectNIidPassport=async () => {
    ImagePicker.showImagePicker({onData: true,mediaType:'photo'},(response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {


        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          nidSource: response.uri,
        });
      }
    });
  };
  selectApplicationForm=async () => {
    ImagePicker.showImagePicker({onData: true,mediaType:'photo'},(response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {


        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          aFormSource: response.uri,
        });
      }
    });
  };
  onValueChange2(value: string) {
    this.setState({
      selected2: value
    });
  }
  onNextStep = () => {
    if (!this.state.isValid) {
      this.setState({ errors: true });
    } else {
      this.setState({ errors: false });
    }
  };
  agentRegister = () => {

    const {first_name,contact_no,email,gender,date_of_birth,password} = this.state;

    const {
    nid_or_passport,
    business_name,
    trade_licenseno,
    regional,
    permanent_address,
    present_address,
    ref1_name,
    ref1_address,
    ref1_contact_no,
    ref1_relation,
    agent_rcv_amount,
      agentSource,
      tdlicenseSource,
      agreementSource,
      nidSource,
      aFormSource


  } = this.state;
    // var newDate = moment().format(date_of_birth, 'YYYY-MM-DD');

    let user={
      first_name:first_name,
      contact_no:contact_no,
      email:email,
      date_of_birth:  date_of_birth,
      gender:gender,
      password:password,


    }

    alert(gender)
    let agentData={
      permanent_address:permanent_address,
      present_address:present_address,
      nid_or_passport:nid_or_passport,
      business_name:business_name,
      trade_licenseno:trade_licenseno,
      regional:regional,
      ref1_name:ref1_name,
      ref1_address:ref1_address,
      ref1_contact_no:ref1_contact_no,
      ref1_relation:ref1_relation,
      agent_rcv_amount:agent_rcv_amount,
    }
console.log(user,agentData)
    const formData = new FormData();

    formData.append('user', JSON.stringify(user));
    formData.append('agentData', JSON.stringify(agentData));

    if(agentSource!='') {

      formData.append('agentfile', {
        uri: agentSource,
        name: `photo.jpeg`,
        type: `image/jpeg`
      });

    }
    if(tdlicenseSource!='') {
//tdlicenseSource=experfile
      formData.append('experfile', {
        uri: tdlicenseSource,
        name: `photo.jpeg`,
        type: `image/jpeg`
      });

    }
    if(agreementSource!='') {

      formData.append('agreementfile', {
        uri: agreementSource,
        name: `photo.jpeg`,
        type: `image/jpeg`
      });

    }
    if(nidSource!='') {

      formData.append('nidfile', {
        uri: nidSource,
        name: `photo.jpeg`,
        type: `image/jpeg`
      });

    }
    if(aFormSource!='') {

      formData.append('applicationfile', {
        uri: aFormSource,
        name: `photo.jpeg`,
        type: `image/jpeg`
      });

    }

  };

  onPrevStep = () => {
    console.log('called previous step');
  };


  componentWillReceiveProps(newProps) {
    const {first_name} = this.state;
alert(first_name)
      return this.setState({
        first_name: first_name,
      });

  }

  render() {
    const progressStepsStyle = {
      activeStepIconBorderColor: '#4c4c4c',
      activeLabelColor: '#4c4c4c',
      activeStepNumColor: 'white',
      activeStepIconColor: '#7d96fb',
      completedStepIconColor: '#13ea6a',
      completedProgressBarColor: '#f75f62',
      completedCheckColor: '#f75f62'
    };

    const buttonTextStyle = {
      color: '#686868',
      fontWeight: 'bold'
    };
    return (
      <Container style={styles.container}>
        <Header
          noShadow
          iosBarStyle={"dark-content"}
          androidStatusBarColor={"#fff"}
          style={{borderBottomWidth: 1}}>
          <Left style={styles.headerLeft}>
            <Button transparent onPress={() => this.props.navigation.navigate("Login")}>
              <Icon name="arrow-back" style={{color: "#000"}}/>
            </Button>
          </Left>
          <Body style={styles.headerBody}>
          <Title style={styles.textBody}>Agent</Title>
          </Body>
          <Right style={styles.headerRight}/>
        </Header>
        <Content>
          <Card>
            <CardItem header bordered>
              <Text>Agent Form</Text>
            </CardItem>
            <CardItem>

              <View style={{ flex: 1, marginTop: 20 }}>
                <ProgressSteps {...progressStepsStyle}>
                  <ProgressStep
                    label="Agent Information"
                    onNext={this.onNextStep}

                    onPrevious={this.onPrevStep}
                    centerContainer
                    nextBtnTextStyle={buttonTextStyle}
                    previousBtnTextStyle={buttonTextStyle}
                  >
                    <ScrollView>
                      <Form>

                        <View style={{ flex:1 }}>

                          <Item regular style={styles.input}>
                            <Input
                            placeholder='Full Name'
                                      onChangeText={(first_name) => this.setState({first_name})}
                                      validate={required}
                            />
                          </Item>

                          <Item regular style={styles.input}>
                            <Input
                              placeholder='Email'
                                    onChangeText={(email) => this.setState({email})}
                            />
                          </Item>


                          <DatePicker
                            style={{width: '100%'}}
                            date={this.state.date} //initial date from state
                            mode="date" //The enum of date, datetime and time
                            placeholder="Date of birth"
                            format="DD-MM-YYYY"
                            minDate="01-01-1980"
                            maxDate="01-01-2020"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                              dateIcon: {
                                position: 'relative',
                                left: 0,
                                top: 4,
                                marginLeft: 2,

                              },
                              dateInput: {
                                marginLeft: 2,
                                marginTop:8
                              }
                            }}
                            onDateChange={(date) => {this.setState({date: date})}}
                          />





                          <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="arrow-down" />}
                            style={{ width: undefined }}
                            placeholder="Select your SIM"
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.selected2}
                            onValueChange={this.onValueChange2.bind(this)}
                          >
                            <Picker.Item label="Male" value="key0" />
                            <Picker.Item label="Female" value="key1" />
                          </Picker>

                          <Item regular style={styles.input}>
                            <Input placeholder='Contact Number'
                                    onChangeText={(contact_no) => this.setState({contact_no})}
                          />
                          </Item>

                          <Item regular style={styles.input}>
                            <Input placeholder='NID/Passport Number'
                                    onChangeText={(nid_or_passport) => this.setState({nid_or_passport})}

                          />
                          </Item>

                          <Item regular style={styles.input}>
                            <Input placeholder='Business Name'
                                    onChangeText={(business_name) => this.setState({business_name})}

                          />
                          </Item>

                          <Item regular style={styles.input}>
                            <Input placeholder='Trade License Number'
                                    onChangeText={(trade_licenseno) => this.setState({trade_licenseno})}
                          />
                          </Item>

                          <Item regular style={styles.input}>
                            <Input placeholder='Regional Office ID Number'
                                    onChangeText={(regional) => this.setState({regional})}
                          />
                          </Item>

                          <Textarea rowSpan={5}
                                    bordered placeholder="Permanent Address"
                                    onChangeText={(permanent_address) => this.setState({permanent_address})}
                          />
                          <Textarea rowSpan={5}
                                    bordered placeholder="Present Address"
                                    onChangeText={(present_address) => this.setState({present_address})}
                          />
                          <View>
                            <View style={{width:"100%",height:'45%',}}>
                              {
                                this.state.agentSource &&
                                <Image source={{uri:this.state.agentSource}}
                                       style={{width:"100%",height:'45%',resizeMode: 'center',}} />
                              }
                              <View style={styles.btnuser}>
                                <Button full success
                                        onPress={this.selectAgent}>
                                  <Icon name='md-cloud-upload' />
                                  <Text>Select user Photo</Text>
                                  {/*onPress={this.selectUser}*/}
                                </Button>
                              </View>
                            </View>


                          </View>
                        </View>

                      </Form>
                    </ScrollView>
                  </ProgressStep>
                  <ProgressStep
                    label="Reference Information"
                    onNext={this.onNextStep}
                    onPrevious={this.onPrevStep}
                    centerContainer
                    nextBtnTextStyle={buttonTextStyle}
                    previousBtnTextStyle={buttonTextStyle}
                  >
                    <ScrollView>
                      <Form>

                        <View style={{ flex:1 }}>
                          <Item regular style={styles.input}>
                            <Input placeholder="Reference Name"
                                      onChangeText={(ref1_name ) => this.setState({ref1_name })}
                            />
                          </Item>
                          <Textarea rowSpan={4}
                                    bordered placeholder="Present Address"
                                    onChangeText={(ref1_address ) => this.setState({ref1_address })}
                          />
                          <Item regular style={styles.input}>
                            <Input placeholder="Contact Number"
                                    onChangeText={(ref1_contact_no  ) => this.setState({ref1_contact_no  })}
                          />
                          </Item>
                          <Item regular style={styles.input}>
                            <Input placeholder="Relation"
                                    onChangeText={(ref1_relation  ) => this.setState({ref1_relation  })}
                          />
                          </Item>

                        </View>
                      </Form>
                    </ScrollView>
                  </ProgressStep>
                  <ProgressStep
                    label="Other Information"
                    onSubmit={this.agentRegister}
                    onPrevious={this.onPrevStep}
                    centerContainer
                    nextBtnTextStyle={buttonTextStyle}
                    previousBtnTextStyle={buttonTextStyle}
                  >
                    <ScrollView>
                      <Form>
                        <View style={{ flex:1 }}>
                          <Item regular style={styles.input}>
                            <Input placeholder="Agent Receivable Amount"
                                onChangeText={(agent_rcv_amount) => this.setState({agent_rcv_amount})}
                      />
                          </Item>
                          <Item regular style={styles.input}>
                            <Input placeholder="Password"
                                    secureTextEntry={true}
                                    onChangeText={(password) => this.setState({password})}
                          />
                          </Item>
                          <Item regular style={styles.input}>
                            <Input placeholder="Retype Password"
                                    secureTextEntry={true}
                                    onChangeText={(re_password) => this.setState({re_password})}

                          />
                          </Item>

                          <View style={{flex:1}}>
                            <View>
                              {
                                this.state.tdlicenseSource &&
                                <Image source={{uri:this.state.tdlicenseSource}}
                                       style={{width:"100%",height:'50%',resizeMode: 'center',}} />
                              }
                              <View style={styles.btnuser}>
                                <Button full success
                                        onPress={this.selectAgentTdLicense}>
                                  <Icon name='md-cloud-upload' />
                                  <Text>Select Trade License</Text>
                                  {/*onPress={this.selectUser}*/}
                                </Button>
                              </View>
                            </View>
                          </View>

                          <View style={{flex:1}}>
                            <View>
                              {
                                this.state.agreementSource &&
                                <Image source={{uri:this.state.agreementSource}}
                                       style={{width:"100%",height:'50%',resizeMode: 'center',}} />
                              }
                              <View style={styles.btnuser}>
                                <Button full success
                                        onPress={this.selectAgreementCopy}>
                                  <Icon name='md-cloud-upload' />
                                  <Text>Select Agreement Copy</Text>
                                  {/*onPress={this.selectUser}*/}
                                </Button>
                              </View>
                            </View>
                          </View>
                          <View style={{flex:1}}>
                            <View>
                              {
                                this.state.nidSource &&
                                <Image source={{uri:this.state.nidSource}}
                                       style={{width:"100%",height:'50%',resizeMode: 'center',}} />
                              }
                              <View style={styles.btnuser}>
                                <Button full success
                                        onPress={this.selectNIidPassport}>
                                  <Icon name='md-cloud-upload' />
                                  <Text>Select NID/Passport</Text>
                                  {/*onPress={this.selectUser}*/}
                                </Button>
                              </View>
                            </View>
                          </View>
                          <View style={{flex:1,paddingBottom: 30}}>
                            <View>
                              {
                                this.state.aFormSource &&
                                <Image source={{uri:this.state.aFormSource}}
                                       style={{width:"100%",height:'50%',resizeMode: 'center',}} />
                              }
                              <View style={styles.btnuser}>
                                <Button full success
                                        onPress={this.selectApplicationForm}>
                                  <Icon name='md-cloud-upload' />
                                  <Text>Select Application Form</Text>
                                  {/*onPress={this.selectUser}*/}
                                </Button>
                              </View>
                            </View>
                          </View>

                        </View>
                      </Form>
                    </ScrollView>
                  </ProgressStep>
                </ProgressSteps>
              </View>

            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}
