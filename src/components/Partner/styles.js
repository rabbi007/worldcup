export default {

  SectionStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderWidth: .5,
    borderColor: '#000',
    height: 45,
    borderRadius: 5 ,
    margin: 7,
    justifyContent: 'space-around'
  },
  input:{
    flex: 1
  },


  btnview:{
    marginTop:2,
    marginLeft:5,



  },
  // button: {
  //   borderColor: '#9B9B9B',
  //   borderWidth: 1 / PixelRatio.get(),
  //   margin: 5,
  //   padding: 5
  // },
  // fileInfo: {
  //   borderColor: '#9B9B9B',
  //   borderWidth: 1 / PixelRatio.get(),
  //   margin: 5,
  //   padding: 5
  // },
  genderpicker:{
    height: 50,
    width: 118,

  },
  text:{
    fontSize: 20,
    color: '#000',
    paddingLeft:5,

  },
  documentBtn:{
    marginTop:5
  },
  btnuser:{
    marginTop:2,
    marginLeft:5,

  },
  item:{
    marginTop:3
  }
};
