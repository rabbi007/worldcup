import React, { Component } from "react";
import {
  Header,
  Left,
  Container,
  Body,
  Title,
  Right,
  Picker,
  Icon,
  Text,
  Item,
  Input,
  Content,
  View,
  Button,
  CardItem,
  Textarea
} from "native-base";

import {
  StatusBar,
  TextInput,
  ScrollView,
  AsyncStorage,
  Image,

  TouchableOpacity,
} from "react-native";
import styles from "./styles";
import { ProgressSteps, ProgressStep } from 'react-native-progress-steps';
import DatePicker from 'react-native-datepicker';
import PhoneInput from 'react-native-phone-input';
import CountryPicker from 'react-native-country-picker-modal';
import ImagePicker from "react-native-image-picker";
import moment from 'moment';






export default class Partner extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {

      state: 'Male',
      phone_no:'',
      first_name: '',
      date_of_birth:'',
      cdSource:'',
      userSource:'',
      password:'',
      NidSource:'',
      gender: undefined,
      dLicenseSource:'',
      vehicleSource:'',
      token:''
    };
    this.state = {date:"01-01-2001"}


  }

  onNextStep = () => {
    if (!this.state.isValid) {
      this.setState({ errors: true });
    } else {
      this.setState({ errors: false });
    }
  };
  partnerRegister = () => {


// alert(AsyncStorage.getItem('token'))
    //alert(this.state.phone.getValue)
    //constant variables that equal properties in state
    const {first_name,phone_no,email,gender,date_of_birth,password,userSource} = this.state;

    const {
      token,
      present_address,
      permanent_address,
      car_nummber,
      tax_token_no,
      fitness,
      insurance,
      agent_id,
      nid_or_passport,
      driving_licenseno,
      dLicenseSource,
      NidSource
    } = this.state;

    var newDate = moment().format(date_of_birth, 'YYYY-MM-DD');

    let user={
      first_name:first_name,
      phone_no:phone_no,
      email:email,
      date_of_birth:  date_of_birth,
      gender:gender,
      password:password,


    }

alert(gender)
    let partnerData={
      permanent_address:permanent_address,
      present_address:present_address,
      car_nummber:car_nummber,
      fitness:fitness,
      tax_token_no:tax_token_no,
      insurance:insurance,
      agent_id:agent_id,
      nid_or_passport:nid_or_passport,
      driving_licenseno:driving_licenseno,
      partner_rcv_amount:0

    }


    // const {TextInputEmail} = this.state;
    // const {TextInputPhoneNumber} = this.state;
    // const {iamgeSource} = this.state;

     const formData = new FormData();
    // //Add your input data
    formData.append('user', JSON.stringify(user));
    formData.append('partnerData', JSON.stringify(partnerData));
    //formData.append('phone_number', TextInputPhoneNumber);
    //
    if(userSource!='') {

      formData.append('userphoto', {
        uri: userSource,
        name: `photo.jpeg`,
        type: `image/jpeg`
      });

    }
    if(dLicenseSource!='') {

      formData.append('liciencefile', {
        uri: dLicenseSource,
        name: `photo.jpeg`,
        type: `image/jpeg`
      });

    }
    if(NidSource!='') {

      formData.append('nidfile', {
        uri: NidSource,
        name: `photo.jpeg`,
        type: `image/jpeg`
      });

    }
    //alert(fileExtension)
    //
    // //API that use fetch to input data to database via backend php script
     fetch('http://192.168.0.103:8000/partner-user-info/',{
       method: 'POST',
       headers: {
         'Content-Type': 'multipart/form-data',
         'Authorization': ' Token '+token,
       },
       body: formData
     })
      .then((response) => response.json())
      .then((responseJson) => {
        // return responseJson
        alert(responseJson.message);
        this.props.navigation.navigate('Partner');
      })
      .catch((error) => {

        alert(error);
      });
    // fetch('http://192.168.0.103:8000/partner-pending-user/')
    //   .then((response) => response.json())
    //   .then((responseJson) => {
    //     alert(responseJson);
    //   })
    //   .catch((error) => {
    //     alert(error);
    //   });
    // alert('zia123' +
    //   '!!');


  };

  onPrevStep = () => {
    console.log('called previous step');
  };


  onValueChange(value) {
    this.setState({
      selected: value
    });
  }

  componentDidMount() {

    AsyncStorage.getItem('token').then((usertoken) =>{
      this.setState({'token' : usertoken});

    })
  }

  // onPressFlag() {
  //   this.countryPicker.openModal();
  // }
  //
  // selectCountry(country) {
  //   this.phone.selectCountry(country.cca2.toLowerCase());
  //   this.setState({ cca2: country.cca2 });
  // }
  selectUser=async () => {
    ImagePicker.showImagePicker({onData: true,mediaType:'photo'},(response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {


        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          userSource: response.uri,
        });
      }
    });
  };
  selectVehicle=async () => {
    ImagePicker.showImagePicker({onData: true,mediaType:'photo'},(response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {


        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          vehicleSource: response.uri,
        });
      }
    });
  };
  selectNid=async () => {
    ImagePicker.showImagePicker({ onData: true, mediaType: 'photo' }, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {


        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          NidSource: response.uri,
        });
      }
    });
  };
  selectDLicense=async () => {
    ImagePicker.showImagePicker({onData: true,mediaType:'photo'},(response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {


        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          dLicenseSource: response.uri,
        });
      }
    });
  };
  render() {
    const progressStepsStyle = {
      activeStepIconBorderColor: '#13ea6a',
      activeLabelColor: '#13ea6a',
      activeStepNumColor: 'white',
      activeStepIconColor: '#a4d4a5',
      completedStepIconColor: '#13ea6a',
      completedProgressBarColor: '#a4d4a5',
      completedCheckColor: '#4bb543'
    };

    const buttonTextStyle = {
      color: '#686868',
      fontWeight: 'bold'
    };
    return (
      <Container style={styles.container}>
        <StatusBar translucent={false}/>
        <Header
          noShadow
          iosBarStyle={"dark-content"}
          androidStatusBarColor={"#fff"}
          style={{borderBottomWidth: 1}}>
          <Left style={styles.headerLeft}>
            <Button transparent onPress={() => this.props.navigation.navigate("Home")}>
              <Icon name="arrow-back" style={{color: "#000"}}/>
            </Button>
          </Left>
          <Body style={styles.headerBody}>
          <Title style={styles.textBody}>Partner</Title>
          </Body>
          <Right style={styles.headerRight}/>
        </Header>
        <Content>
          <CardItem>
          <View style={{ flex: 1, marginTop: 20 }}>
            <ProgressSteps {...progressStepsStyle}>
              <ProgressStep
                label="Personal Information"
                onNext={this.onNextStep}
                onPrevious={this.onPrevStep}
                centerContainer
                nextBtnTextStyle={buttonTextStyle}
                previousBtnTextStyle={buttonTextStyle}
              >

                <ScrollView>
                  <View style={{ flex:1 }}>

                    <Item regular style={styles.item}>
                      <Input
                        style={styles.input}
                        placeholder='Full Name'
                        onChangeText={(first_name) => this.setState({first_name})}
                        placeholderTextColor='black'
                        autoCapitalize="none"
                        underlineColorAndroid="transparent"
                      />
                    </Item>


                    <Item regular style={styles.item}>
                      <Input
                        style={styles.input}
                        placeholder='Email'
                        placeholderTextColor='black'
                        autoCapitalize="none"
                        onChangeText={(email) => this.setState({email})}
                        underlineColorAndroid="transparent"
                      />
                    </Item>


                  <Picker
                    mode="dropdown"
                    placeholder="Select Gender"
                    iosIcon={<Icon name="arrow-down" />}
                    textStyle={{ color: "#5cb85c" }}
                    itemStyle={{
                      backgroundColor: "#d3d3d3",
                      marginLeft: 0,
                      paddingLeft: 10
                    }}
                    itemTextStyle={{ color: '#788ad2' }}
                    style={{ width: undefined }}
                    selectedValue={this.state.gender}
                    onValueChange={this.onValueChange.bind(this)}
                    >
                    <Picker.Item label="Male" value="1" />
                    <Picker.Item label="Female" value="2" />
                  </Picker>

                  <DatePicker
                    style={{width: '100%'}}
                    date={this.state.date_of_birth} //initial date from state
                    mode="date" //The enum of date, datetime and time
                    placeholder="Date of birth"
                    format="YYYY-MM-DD"
                    minDate="01-01-1980"
                    maxDate="01-01-2020"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'relative',
                        left: 0,
                        top: 4,
                        marginLeft: 0,
                        marginRight:0
                      },
                      dateInput: {
                        marginLeft: 4,
                        marginRight: 0
                      }
                    }}
                    onDateChange={(date_of_birth) => {this.setState({date_of_birth: date_of_birth})}}
                  />


                  <Item regular style={styles.item}>
                    <Input placeholder='Contact Number' name="phone_no"
                            onChangeText={(phone_no) => this.setState({phone_no})}
                     />
                  </Item>


                  {/*  <PhoneInput*/}
                  {/*    placeholder='phone'*/}
                  {/*    onChangeText={ phone_no => this.setState({ phone_no }) }*/}

                  {/*    style={{paddingLeft:7}}*/}
                  {/*    ref={(ref) => {*/}
                  {/*      this.phone = ref;*/}
                  {/*    }}*/}
                  {/*    onPressFlag={this.onPressFlag}*/}
                  {/*  />*/}

                  {/*<CountryPicker*/}
                  {/*  ref={(ref) => {*/}
                  {/*    this.countryPicker = ref;*/}
                  {/*  }}*/}
                  {/*  onChange={value => this.selectCountry(value)}*/}
                  {/*  translation="eng"*/}
                  {/*  cca2={this.state.cca2}*/}
                  {/*>*/}

                  {/*</CountryPicker>*/}

                  {/*<Item regular>*/}
                  {/*  <Input placeholder='Contact Number' name="phone_no"  secureTextEntry={true}  onChangeText={(phone_no) => this.setState({phone_no})}/>*/}
                  {/*</Item>*/}
                  <Item regular style={styles.item}>
                    <Input placeholder='Password' name="password" type="password" secureTextEntry
                           onChangeText={(password) => this.setState({password})}/>
                  </Item>



                    <Item regular style={styles.item}>
                      <Input placeholder='Retype Password' name="re_password" type="password" secureTextEntry
                             onChangeText={(re_password) => this.setState({re_password})}/>
                    </Item>


                    <Item regular style={styles.item}>
                      <Input placeholder='Fitness Validity' name="fitness"
                             onChangeText={(fitness) => this.setState({fitness})}/>
                    </Item>
                    <Item regular style={styles.item}>
                      <Input placeholder='Partner Receivable Amount' name="partner_rcv_amount"
                             onChangeText={(partner_rcv_amount) => this.setState({partner_rcv_amount})}/>
                    </Item>
                    <Item regular style={styles.item}>
                      <Input
                        style={styles.input}
                        placeholder='NID/Passport Number'
                        ref='mobileNo'
                        keyboardType="numeric"
                        placeholderTextColor='black'
                        autoCapitalize="none"
                        underlineColorAndroid="transparent"
                        onChangeText={(nid_or_passport) => this.setState({nid_or_passport})}
                      />
                    </Item>
                    <Item regular style={styles.item}>
                      <Input
                        style={styles.input}
                        placeholder='Agent ID'
                        ref='mobileNo'
                        keyboardType="numeric"
                        placeholderTextColor='black'
                        autoCapitalize="none"
                        underlineColorAndroid="transparent"
                        onChangeText={(agent_id) => this.setState({agent_id})}
                      />
                    </Item>
                    <Item regular style={styles.item}>
                      <Input
                        style={styles.input}
                        placeholder='Driving License'
                        ref='mobileNo'
                        keyboardType="numeric"
                        placeholderTextColor='black'
                        autoCapitalize="none"
                        underlineColorAndroid="transparent"
                        onChangeText={(driving_licenseno) => this.setState({driving_licenseno})}
                      />
                    </Item>

                      <Textarea
                        rowSpan={5}
                        bordered
                        style={styles.input}
                        placeholder='Present Address'
                        placeholderTextColor='black'
                        autoCapitalize="none"
                        underlineColorAndroid="transparent"
                        onChangeText={(permanent_address) => this.setState({permanent_address})}
                      />


                      <Textarea
                        rowSpan={5}
                        bordered
                        style={styles.input}
                        placeholder='Permanent Address'
                        placeholderTextColor='black'
                        autoCapitalize="none"
                        underlineColorAndroid="transparent"
                        onChangeText={(present_address) => this.setState({present_address})}
                      />

                    <View style={{flex:1}}>
                      {
                        this.state.userSource &&
                        <Image source={{uri:this.state.userSource}}
                               style={{width:"100%",height:'45%',resizeMode: 'center',}} />
                      }
                      <View style={styles.btnuser}>

                        <Button full success
                                onPress={this.selectUser}>
                          <Text>Select user Photo</Text>
                          <Icon name='md-cloud-upload' />
                        </Button>
                      </View>
                    </View>

                  </View>
                </ScrollView>

              </ProgressStep>


              <ProgressStep
                label="Vehicle Information"
                onNext={this.onNextStep}
                onPrevious={this.onPrevStep}
                centerContainer
                nextBtnTextStyle={buttonTextStyle}
                previousBtnTextStyle={buttonTextStyle}
              >
                <ScrollView>
                  <View style={{flex:1}}>
                    <Item regular style={styles.item}>
                      <Input
                        style={styles.input}
                        placeholder='Car Number'
                        placeholderTextColor='black'
                        autoCapitalize="none"
                        underlineColorAndroid="transparent"
                        onChangeText={(car_nummber) => this.setState({car_nummber})}
                      />
                    </Item>
                    <Item regular style={styles.item}>
                      <Input
                        style={styles.input}
                        placeholder='Tax Token Number'
                        keyboardType="numeric"
                        placeholderTextColor='black'
                        autoCapitalize="none"
                        underlineColorAndroid="transparent"
                        onChangeText={(tax_token_no) => this.setState({tax_token_no})}
                      />
                    </Item>


                    <Item regular style={styles.item}>
                      <Input
                        style={styles.input}
                        placeholder='Insurance'
                        placeholderTextColor='black'
                        autoCapitalize="none"
                        underlineColorAndroid="transparent"
                        onChangeText={(insurance) => this.setState({insurance})}
                      />
                    </Item>
                    <View>
                      {
                        this.state.vehicleSource &&
                        <Image source={{uri:this.state.vehicleSource}}
                               style={{width:"100%",height:'100%',resizeMode: 'center'}} />
                      }
                      <View style={styles.btnview}>
                        <Button full success
                                onPress={this.selectVehicle}>
                          <Text>Select Vehicle Photo</Text>
                          <Icon name='md-cloud-upload' />
                        </Button>

                      </View>
                    </View>

                    <View style={{
                      resizeMode: 'center',
                      flexDirection:'row',
                      backgroundColor: '#7993b3',
                      width:100,
                      marginTop:10,
                      alignItems: 'center',

                    }}>
                      <TouchableOpacity>
                        <Text style={styles.text}>Add More</Text>
                      </TouchableOpacity>
                    </View>

                  </View>
                </ScrollView>
              </ProgressStep>


              <ProgressStep
                label="Other Information"
                onSubmit={this.partnerRegister}
                onPrevious={this.onPrevStep}
                centerContainer
                nextBtnTextStyle={buttonTextStyle}
                previousBtnTextStyle={buttonTextStyle}
              >
                <ScrollView>

                  <View style={{flex:1}}>
                    <Item regular style={styles.item}>
                      <Input
                        placeholder='Owner Name'

                      />
                    </Item>
                    <Item regular style={styles.item}>
                      <Input
                        placeholder='Contact Number'

                      />
                    </Item>
                    <Item regular style={styles.item}>
                      <Input
                        placeholder='NID/passport Number'
                      />
                    </Item>
                    <Textarea
                      rowSpan={5}
                      bordered
                      placeholder='Address'
                      onChangeText={(permanent_address) => this.setState({permanent_address})}
                    />

                    <View style={styles.documentBtn}>
                      {
                        this.state.NidSource &&
                        <Image source={{uri:this.state.NidSource}}
                               style={{width:"100%",height:'40%',resizeMode: 'center'}} />
                      }
                      <View style={styles.btnview}>
                        <Button
                          full success
                          onPress={this.selectNid} >
                          <Text>Select NID/Passport</Text>
                          <Icon name='md-cloud-upload' />
                        </Button>
                      </View>
                    </View>

                    <View style={styles.documentBtn}>
                      {
                        this.state.dLicenseSource &&
                        <Image source={{uri:this.state.dLicenseSource}}
                               style={{width:"100%",height:'40%',resizeMode: 'center'}} />
                      }
                      <View style={styles.btnview}>
                        <Button
                          full success
                          onPress={this.selectDLicense}>
                          <Text>Select Driving License</Text>
                          <Icon name='md-cloud-upload' />
                        </Button>

                      </View>
                    </View>

                    {/*<View style={styles.documentBtn}>*/}
                    {/*{*/}
                    {/*this.state.cdSource &&*/}
                    {/*<Image source={{uri:this.state.cdSource}}*/}
                    {/*style={{width:"100%",height:'40%',resizeMode: 'center'}} />*/}
                    {/*}*/}
                    {/*<View style={styles.btnview}>*/}
                    {/*<Button*/}
                    {/*color="#BCBCBC"*/}
                    {/*title="Select Car all Document"*/}
                    {/*onPress={this.selectCD} />*/}
                    {/*</View>*/}
                    {/*</View>*/}

                  </View>
                </ScrollView>
              </ProgressStep>
            </ProgressSteps>
          </View>
          </CardItem>
        </Content>
      </Container>
    );
  }
}
