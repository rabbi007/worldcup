import React, {Component} from "react";
import {
  Header,
  Left,
  Container,
  Body,
  Title,
  Right,
  Icon,
  Text,
  Card,
  CardItem,
  Content, View, Button
} from "native-base";
import {
  StatusBar,
  TextInput,
} from "react-native";
import styles from "./styles";


export default class MapView extends Component {

  render() {


    return (
      <Container style={styles.container}>
        <StatusBar translucent={false}/>
        <Header
          noShadow
          iosBarStyle={"dark-content"}
          androidStatusBarColor={"#fff"}
          style={{borderBottomWidth: 1}}>
          <Left style={styles.headerLeft}>
            <Button transparent onPress={() => this.props.navigation.navigate("Home")}>
              <Icon name="arrow-back" style={{color: "#000"}}/>
            </Button>
          </Left>
          <Body style={styles.headerBody}>
            <Title style={styles.textBody}>Location Finding</Title>
          </Body>
          <Right style={styles.headerRight}/>
        </Header>
        <Content>
          <View style={{flex: 1, marginTop: 10}}>
            <Card>
              <CardItem bordered>
                <Icon name="map"/>
                <TextInput placeholder="Pickup Your Location">

                </TextInput>

              </CardItem>
              <CardItem bordered>
                <Icon name="map"/>
                <TextInput placeholder="Destination">

                </TextInput>
              </CardItem>
            </Card>


            <Right>
              <Button iconLeft light onPress={() => alert("Hi")}>
                <Icon name="search"/>
                <Text>Search</Text>
              </Button>
            </Right>


          </View>
        </Content>

      </Container>
    );
  }
}
