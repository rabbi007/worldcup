import * as ACTION_TYPES from "../constants/action-types";
import {AsyncStorage} from 'react-native';
import {LOGIN_URL} from "../constants/apis"

export const userLoginSuccess = ({ email, password }) => ({
  types: [
    ACTION_TYPES.USER_LOGIN_REQUESTED,
    ACTION_TYPES.USER_LOGIN_SUCCESS,
    ACTION_TYPES.USER_LOGIN_FAILED
  ],
  payload: {
    client: "default",
    request: {
      method: "POST",
      url: "/login-success",
      data: {
        email,
        password
      }
    }
  }
});

export const userLoginFail = ({ email, password }) => ({
  types: [
    ACTION_TYPES.USER_LOGIN_REQUESTED,
    ACTION_TYPES.USER_LOGIN_SUCCESS,
    ACTION_TYPES.USER_LOGIN_FAILED
  ],
  payload: {
    client: "default",
    request: {
      method: "POST",
      url: "/login-fail",
      data: {
        email,
        password
      }
    }
  }
});



// export const userLogin = ({username,password}) => {
//   console.log(password,username)
//   alert(username)
  
// }

export function userLogin(username, password) {

  return (dispatch) => {
    fetch(LOGIN_URL,{
      method:"POST",
      headers:{
         "content-type":"application/json"
      },
      body:JSON.stringify({username,password})
   })
   .then(res=>res.json())
   .then(json => {
     if(json.error){
      // dispatch({
      //   type: ACTION_TYPES.USER_LOGIN_FAILED
      // })
        alert(json.error)
     }
     // else if(json.roleId != 2){
     //  // dispatch({
     //  //   type: ACTION_TYPES.USER_LOGIN_FAILED
     //  // })
     //  alert("You are not agent user !")
     // }
     else{
      // AsyncStorage.setItem('first_name', "Test"),

      AsyncStorage.multiSet([
        ["first_name",json.token],
        ["last_name", json.last_name],
        ["phone_no",username],

      ])
    AsyncStorage.setItem("token",json.token)

       dispatch({
        type: ACTION_TYPES.USER_LOGIN_SUCCESS,
        payload: json
      })

    
     }
   })
}

}



function userLoginRequest() {
  return {
      type: ACTION_TYPES.USER_LOGIN_REQUESTED,
  }
}

function userLoginSuccessType(data) {
  return {
      type:  ACTION_TYPES.USER_LOGIN_SUCCESS,
      data: "Zia"
  }
}

function userLoginError(error) {
  
  return {
      type:  ACTION_TYPES.USER_LOGIN_FAILED,
      error: error
  }
}



  



export function userRequestLogout() {
  AsyncStorage.clear()
  return {
    type: ACTION_TYPES.USER_LOGOUT_REQUESTED,
    payload: {},
  };
}
