import React from "react";
import { createDrawerNavigator } from "react-navigation";
import SideBar from "../containers/SlidebarContainer";
import Home from "../containers/HomeContainer";
import Modal from "../containers/ModalContainer";
import Partner from "../containers/PartnerContainer";
import MapView from "../containers/MapviewContainer"
import Agent from "../containers/AgentContainer";

export default createDrawerNavigator(
  {
    Home: { screen: Home },
    Partner: { screen: Partner },
    MapView:{ screen:MapView },
    Agent:{ screen:Agent },
  },
  {
    initialRouteName: "Home",
    contentComponent: props => <SideBar {...props} />
  }
);
